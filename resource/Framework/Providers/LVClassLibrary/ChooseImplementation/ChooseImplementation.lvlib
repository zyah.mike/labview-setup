﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)N!!!*Q(C=\&gt;3^&lt;?.!%-8RZY/$3^8"13V-#YK&gt;K96J1;F$N@!!6K!7V-+UI"&lt;5!O`0Z&gt;C1!UM8H!%(8HIJ[OX84UO;UCC`J7@.D]JUN[TNS\EPZGF=T^-[&gt;$WNS6O@&lt;F_P&lt;O0XC4Y&lt;0UX4@$0_Y`LT\:C0\@@]D]L,X@,4`N0_P&gt;M@&amp;.Y$LY``#&lt;[W3%^;8E45IJK;V.$3."&gt;ZE2&gt;ZE2&gt;ZE:P=Z#9XO=F.HO2*HO2*HO2*(O2"(O2"(O2"0AZSE9N=Z/R+M8CR5$&amp;J-5(2'9K+5_%J0)7H]0"6B;@Q&amp;*\#5XDIIM*4?!J0Y3E]$&amp;0B+4S&amp;J`!5(K9;EBI(/:\#Q`1S(O-R(O-R(J;5]2C!7=R-&lt;#;")&gt;.I,IT(?)S(3RG0]2C0]2A0T4)?YT%?YT%?BIR&gt;]&gt;!M"TE?JF(C34S**`%E(K:7YEE]C3@R*"[75_**0!EC74#:()+315G(Z%PC34R]+0%EHM34?")04?-/Z&gt;C:2&lt;-=Z(A#4_!*0)%H]$#&amp;!E`A#4S"*`!QL1*0Y!E]A3@QM*1#4_!*0!%E7*4F&amp;5Q7$!Q["5(AY7]],4(OEI=ERC(VQ[N_+.50G`IB5D]=[JOOPJHKG[4?@07GKD&gt;,P1HK([&gt;'KT(K2&gt;3$FY[[=LZ1T^14^5A^50@5(86,X3R$`X0([`7KS_7C]`GMU_GEY`'IQ_'A`8[PX7[H\8;LT7;DN`+(9SV0``J??K8P6^4VP@1&gt;XIX[J@8T@9`_!N!WDA5!!!!!</Property>
	<Property Name="NI.Lib.Locked" Type="Str">205711D140723D6BBB6516562B9D45C8</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="UtilityVIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CLSUIP_BuildTree.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_BuildTree.vi"/>
		<Item Name="CLSUIP_ClassImplementsMethod.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_ClassImplementsMethod.vi"/>
		<Item Name="CLSUIP_FindImplementation.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_FindImplementation.vi"/>
		<Item Name="CLSUIP_FindLVClassRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_FindLVClassRef.vi"/>
		<Item Name="CLSUIP_GetMethodName.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_GetMethodName.vi"/>
		<Item Name="CLSUIP_IsValidTreeItem.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_IsValidTreeItem.vi"/>
		<Item Name="CLSUIP_MarkImplementorInTree.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_MarkImplementorInTree.vi"/>
		<Item Name="CLSUIP_OpenImplementations.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_OpenImplementations.vi"/>
		<Item Name="CLSUIP_ResizeAndPositionFP.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_ResizeAndPositionFP.vi"/>
		<Item Name="CLSUIP_SaveWindowSize.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_SaveWindowSize.vi"/>
		<Item Name="CLSUIP_Insert Diagram Into Subpanel.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_Insert Diagram Into Subpanel.vi"/>
		<Item Name="CLSUIP_Open Window Error Message.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_Open Window Error Message.vi"/>
		<Item Name="CLSUIP_Handle Tree Multi-Select.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_Handle Tree Multi-Select.vi"/>
		<Item Name="CLSUIP_Restore Diagram Appearance.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_Restore Diagram Appearance.vi"/>
		<Item Name="CLSUIP_Multi Select Message.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_Multi Select Message.vi"/>
		<Item Name="CLSUIP_Sentinel Bounds and Origin.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_Sentinel Bounds and Origin.vi"/>
	</Item>
	<Item Name="Typedefs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CLSUIP_TreeItemInfo.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_TreeItemInfo.ctl"/>
	</Item>
	<Item Name="Glyphs" Type="Folder">
		<Item Name="CLSUIP_BelowHereGlyph.png" Type="Document" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_BelowHereGlyph.png"/>
	</Item>
	<Item Name="CLSUIP_ChooseImplementationDialog.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/ChooseImplementation/CLSUIP_ChooseImplementationDialog.vi"/>
</Library>
<?sig 626C381CA80A5CDF939E583A089DF0B8?>
