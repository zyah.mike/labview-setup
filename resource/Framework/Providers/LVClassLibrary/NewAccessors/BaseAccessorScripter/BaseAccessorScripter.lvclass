﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="" Type="Bool">true</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">MemberVICreation.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/MemberVICreation.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!);!!!*Q(C=\&gt;4.&lt;=*!%-8RFSC(8/EAII68!L2!#^-#VRRJ96KA"6K9&amp;GC"&amp;JS`FUG#C%3E##EZ:*=V],Q@0[]N3[-]3U_;PCOLG_6?Z]],^7&amp;;4:^Z@Z]`Z[]_@"F`U7G[0D^`XC^HOD8_`/N[`:^@X\3[7@6@`MPPFN&gt;PXQ&amp;8Z@[%"]UP)FL2EB9U;VZK+P)C,`)C,`)C4`)E4`)E4`)E$`)A$`)A$`)A.\H*47ZSEZN]6(+2CVTEE)L&amp;CY7+39M*CMZ16"Q+4_%J0)7(PSI]B;@Q&amp;*\#1R=6HM*4?!J0Y7'9#E`B+4S&amp;J`!QV:$5K/2Y#A`4+`%EHM34?")03SLR*)"EM74C:")93EYG0R*0YEE]`&amp;4C34S**`%E(EYL]33?R*.Y%A^$RK\EU-S6(!`4+0!%HM!4?!)05SPQ"*\!%XA#$]MJ]!3?!"%M'%Q/1='AI%0Q*`!%(LY5?!*0Y!E]A9&gt;4YQ\&amp;W*F:-V&gt;S0-:D0-:D0-&lt;$&amp;$)?YT%?YT%?JJ8R')`R')`RM*3-RXC-RU$-ICQP-ZE:;$K:Q(DYD+@&amp;YS\FE(B5K2^?^5/J@ND5$Z([Y6$@&gt;08.6.]E^?;L.V7^7?J.5&amp;_='KX'K"&gt;2$ZY\[M4R3$P1^L1&gt;&lt;5P&lt;U.;U*7UR$\VTR^0JJ/0RK-0BI0V_L^VOJ_VWK]VGI`6[L?6SK=6C]@%;?+&amp;_P"$_QHPJ4RA?&gt;4EP?`1'4'SN=A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6$0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-T9Z.41Y0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YR.D=X.T)R.4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6$0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-T9Z.41Y0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YR.D=X.T)R.4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D-],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#2B5F.31QU+!!.-6E.$4%*76Q!!([1!!!3^!!!!)!!!(Y1!!!!Y!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!!!#A)!#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!-%'5)9HTH&gt;"P:"B9J:Y()Y!!!!-!!!!%!!!!!!W&gt;6P&amp;DF^&amp;1[3=$JH\PZR+V"W-W9]!MA4JA!G9\0B#@A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#D)+#4;5=R?B::*PD%FG&lt;+!!!!"!!!!!!!!!#I!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"%5.-5V6*5&amp;^"9W.F=X-O9X2M5&amp;2)-!!!!&amp;%!!!!'#DRS:8.P&gt;8*D:4Y*2H*B&lt;76X&lt;X*L#6"S&lt;X:J:'6S=QZ-6E.M98.T4'FC=G&amp;S?1R/:8&gt;"9W.F=X.P=H-21UR465F18U&amp;D9W6T=SZD&gt;'Q!!!!$!!&amp;#!!!!!!!$!!!!!A!#!!!!!!!I!!!!)HC=9_"E9'ZAO-!!R)Q/4!V=1":4"A/9:PD!Q-!2Q!!!D&lt;M)6A!!!"1!!!!5?*RDY'$A9?"!A1Q!!X!!41!!!%M!!!%9?*RD9-!%`Y%!3$%S-$$^!.*M;/*A'M;G*M"F,C[\I/,-1-Q#R+QQ9;#\^Q"J*J!Y6)UY2)LJ!2#@1$?((UJ@1")$!,IV+5]!!!!!$!!"6EF%5Q!!!!!!!Q!!!9Y!!!.=?*QL9'2AS$3W-$M!J*G"7)'BA3%Z0S76FQ():Y#!.UQ-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`NIM,38+0#QV4+^\^%B30A"5CQ_1D(Y7[0H///.G!F(&amp;E-71Q"`Q-TGI`QA(5D[=^C!'I4!7'I4BYU]XV57!Q0.&amp;1K-Z1+(W]U993Y)B#KG/5Q1@V!^Q(.Y$DYE+6X)FCA%U2WBD"+((&gt;BV"%$MHNZ'!-2\E:T8VAXU&amp;&gt;:D'%Q_7[WYQY;)0:R"R%)F1'B+C"5!9D;!3,C$G-,V\7P\_VC"&gt;*M3')/5.Q!R+"YB7%^"E9'E)?:A,!$30`Z``_`$6#%#3KG#"5$M7^#W9Q-^H!^M[&amp;C'EDG`)4L1;C,BIIZ),E(:!@)JLV!7A0+0ARF.U$&gt;$R*D"2IS!=LG!&lt;),I'RB)(M$F#U&amp;:!N!W9J!^A=I7QX+0A#.684;W&gt;`&amp;&amp;4HM1/E;FM:6A4AZN]$!1+_[+#V!JSD.(YD$Q.B**T8=RNB/*^J1*TCW&amp;KB/";3W),G-E&amp;)!/KK;O!!!!!!"O!!!!Q"YH(.A9'$).,9Q=W"C9'"G:'"19'BA3-Z0376!!HO9'(##Y0#QZD=SX45K"NUF+DK&gt;*3I3H45K)P`.`ZO^!%EX(_(I0'BYI0EH)`_5A`SN9E!&lt;?NV9/FV5*(L&gt;1:4)`X+7`W5MW.3_!AJVAR2[M(4[K%BU_[C)^!;S&gt;)9!^23S`#`!KG=&lt;'8K;=/JJK'"BY*^[K+%32"XMP'"QQO"5#&lt;M*)`_W![]$)6I9%6I916LYNQ=Q'BZ)MQY%G6WK"6(%"&amp;,5$01M5\=$5_O"%JFOAW[WZG-M/U(BOIM&amp;3,Q7\TD2@%1G,D\[]%Y1X`$!,J#=Y&lt;&gt;/FCG&gt;,$V!9U3['9![R&lt;MNONGN861%;E'"*^"]H%84298D.=&gt;R)!@EN?Y9&amp;9(7WX63S/(@`)/R6"2I'V#R&gt;`UTI,^L6$A9_+==?.U(*&amp;Y;`0``(X@M%A:L8^]$_Y!23=S"!:37'-&amp;C)'^)!`%@I$UP'""C\F!R*19*O.BM2ID93[!)%V3M'KI/R&amp;Y0:4N!\8(W&gt;X&amp;&amp;&gt;AMIH9,-5A8CZ.Q#!Q/^[K+U!*WC.(]A$A.D*ZX5="ND/ZVI1ZXAW&amp;KA/A!5)]A]!!!"&lt;A!!!FRYH(.A9'$).,9Q/]$)Q-!-R!I-$1T*_3GJ$%B!BIE"*QBL@C04\;+CU?WDIN,JIS,2[;)CUMX:S1%5E_DV9!%*!;6%?A.:/E/!-AYMH9YMV*8L=G$JA-ER)O1918,]WRU9GX]QJ?VC9EDD&gt;\K1VBH)#&amp;()"&amp;,9$(1L5X="5_OX%NZOMVYX&amp;K$D?&lt;L:8D-:(A#S",J"8G&amp;I06$#UMX1T7!*&amp;#F2[173P4YK)*5MA@^,6#3SW$*!Q@!#2$1@%7A^5"I&amp;V]@;T&gt;"]!+15&amp;"I#H3QKBN^;$^1LP&lt;:)3^0DFW]"BR`1$4QM1)+$J:N&amp;R1/IJP6!H121/1`1=)\`"F"T/9#G8?U]VPGB_9B-X'(E]&amp;`\_NYO:C$.C#4G!-2:1"'1'#DK%I#-0````_^B-!#,A4!T6%-%EDI/K$I(K$H/`C[O[/E!J&amp;9&amp;C*-,EMPUKIP3!H3+UPS"/!S-H823QWW-\83C$87#9WM"IKS/WA!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!-)!#!!!!!"$)Q,D!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!-)!#!!!!!"$)Q,D!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9$A!!'!E!!"A/.E!9#63A'!Y]Q"A!!!!9!!!!'!!!!"M!!!!&lt;!!!!(*G4&lt;N_C+EK=G&lt;&lt;/G!!!!"A!!!!9!!!!'=!A!"I!!)!:D;T*G&amp;%KF2O.,&amp;E9!!A!'!!!!"A!!!!9!!!!'!!!!"`````Q!!!A$`````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!$`]!!!!!!!!!!!!!`Q!!!!]!]!!!!!!!!!!!!0]!!!!0`Q!0]0]!]!!!!!$`!!!!$Q$Q]0$Q$Q]!!!!!`Q!!!!``!!``]!`Q!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]0]!!!!!!!!!!!!!!!!!$`$`!!!!!!!!!!!!!!!!!!``!0!0]!`Q$Q$`$`$`]0]0```Q]!$Q!0$Q]!]!]0$Q$`]!]!`Q$`$`$`$`!0`Q]!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q$`]!!!!!]!!!!!!!!!!0]0!!!!!!!!!!!0!!!!!!$`!0]!$`$`$Q`Q$`!0!0]!`Q!!]0!!]!]0$Q]!]0$Q!0]0`Q!0]0!0$`!!]0]!]!$`!!!!!!!!!!]!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0`````````````````````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!$```]!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!0]!!0]!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!````!!!!``]!``]!!0]!!!!!!!!!!!$``Q!!!!!!!!$`!!$`!0]!`Q$`!!$`!0]!!!!!!!!!!0``!!!!!!!!!0```Q!!!0````]!!0``!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```Q!!`Q!!``]!!0``!!$`!!$``Q$``Q$```]!``]!````````!0]!!!$`!!!!`Q$`!0]!!0]!!0]!`Q$`!!$```]!!0]!!0``!!$``Q$``Q$``Q$``Q!!````!0]!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!$```]!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!!!``]!`Q!!!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!$``Q!!``]!!!$``Q$``Q$`!0``!!$``Q!!`Q!!``]!!0``!!!!!0]!`Q!!!0]!!0]!`Q$`!0]!!0]!`Q$`!!!!``]!````!!!!``]!`Q!!`Q$``Q!!!0]!``]!!0]!!!$``Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!]A!"2F")5!!!!!%!!F2%1U-!!!!"%5.-5V6*5&amp;^"9W.F=X-O9X2M5&amp;2)-!!!!&amp;%!!!!'#DRS:8.P&gt;8*D:4Y*2H*B&lt;76X&lt;X*L#6"S&lt;X:J:'6S=QZ-6E.M98.T4'FC=G&amp;S?1R/:8&gt;"9W.F=X.P=H-21UR465F18U&amp;D9W6T=SZD&gt;'Q!!!!$!!"#!!!!!!!!!!%!!!"65&amp;2)-!!!!&amp;9!!!!'#DRS:8.P&gt;8*D:4Y*2H*B&lt;76X&lt;X*L#6"S&lt;X:J:'6S=QZ-6E.M98.T4'FC=G&amp;S?1R/:8&gt;"9W.F=X.P=H-7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9A!$!!!!!!!)!!!!!1!!!!!!!!?H!!!8*8C=L6BN&lt;&amp;.6'(Z0&gt;\P&gt;&gt;BO\H?SD@,5LNZ5*Z7/C#+)AOW!W%(6D@%;AL!7'-+$L6+*"IH7[(]11!IEO*0\C&amp;QEG%%Q-0YAJTK4_)*A9":-+0YQ[301(9D"XV`?=U`P:&lt;BUQ3%\+?$_@^XG@=TK!BC;JU45+2V5AUDX]M%Y&amp;&lt;TR,!$*2%@*`)A-A&gt;:(`A%TV%R67CFX34&gt;=IG;&amp;#64Q&lt;%2=KA`"8XJI-QU`3,41NF`Q9T+N#44T&lt;Y'O8=Z*]@I9][.;D_G#G&gt;*S-OD&lt;,A@PC]@2"4!DJ/@4U2=EI%+6*%.+BVW+^C&lt;2-@_K*CHY7UK/#J'2H*O8=ERA25X`$1Q[43ZA[(R)QZ"SY=/'#[?4D4B&amp;7RH0IA`;UX/&amp;R@'L2JU`/T7-_8OYTT&amp;JE0EJA^#3NH4IZ8:^1MB+[IN]\/D38^&amp;R+1&amp;V:V'_P#F0FX#L2,^[+XX"8PZ\_#AC1^$8N4_VG:I=)^2!GX]-&gt;XXI[#_:2C_.1=&amp;LB:54KR(^XKP"5/OP;#1,.'I$FZ!@YG1V%U!?SH!ZE$2O)2RG5Q*T)S2)4592-;'&lt;L`P[_6#):0,A\W,U`VN=80*4M?3O73A4DM63M=&amp;9P+&amp;H`%II$4=:J5A=#_2)_M_*_%-[?09N1Y'G[PICO$8,/]0-BBB@BD)&amp;^X-31:D5R8)%9+P^OW%.R$#^T[?SN9/S.MP-W/T?Q=SY\L\+TCZX\4);X)-.^EL.U']/@G8S'0YP%/_JE_#ES"'@(9?M3\G2B_"!C&gt;AZ/D?0T(0K]&lt;W0Y%/9ZJ_=:D_&amp;,#RE_J/=S'(\[^'EHQZ=:$"=)-2B_#Q6HB[D^IXV.4E)X9XA&amp;]_D!=7!G3F03I6UB!^$&amp;Y0@I]"]OSO&gt;GR0`9_0A(SY(/)"/K;5UGE-(R9&amp;MK=;".[80-YT56DM3TID_JZ!+LZ0/TZ-(&amp;.)?%.P/AG28D:I;`KP"O0#PY57;]&amp;LZ89#G.%.;2U8Q-G1=0(C!S?0*KUEVQ4)#U4(`CC;:4Y@E,&gt;_@T6[EQ"6?B2=[6M;A,79N4-0U-G)+2,F__D*(Q^!7Q&amp;*@E*[.FQ5S!MX!+GA3:32-^J6"_L,P%%!V@;%IHVE20O[EH9&amp;&lt;4DN5]+_&gt;=FGJU`U?MJHPCV84&lt;KE%?(M6KXL&lt;)2"F5)T$.J75#?&lt;]KP!8#7Q(&amp;II?,26FEQ'7'`A"$&lt;Z*TM\$2`',4I=_(?8&lt;N+L)=([*HHVXW'C!+#_'%AQ:&amp;&gt;#^N,,^(8`Y'/'&lt;[=DY\%HZE,,_(,T_+@\3%TY#R`"[_`/BDSX.PL09_RP;[,*A`A@OWW&amp;T_M4(`R)LZ8I[Z%"EI9Z@8[SK:GV^V!SVTO\QK771KL&lt;\KWFU!&lt;22#*;5W%SLP4#6\?P=YNLN3B@?Q'V3F'BZ6J/H&gt;)/$7?L'*HW`]ANXX$XT"_#R1EN)7\X`(#=M_$VM_@WPZ@.8#6#S`B7^RJ955!P[NAZU4U!:"VQ:D#O2JRQ6*``AB8H)+:,&amp;V#F@$\\LY).QY#,%T=IS5H[CNIU]-W1`?ONJ[`7.^&lt;90_M3%@,)Z8*IYM3.7Z'&gt;7Z1V0:S-JU&gt;9\L[KS=$\3&lt;]FS*[,;5E'=8]#&gt;(_5P&gt;X9E_JSIDG2+7.KL8ZH__396$7*'!9NV/=^7QS_!&amp;6F7&amp;8F736L8?4K3`M3A84#V**$E%DF*1^`OI\L@LOM_\&gt;--+@,'^[JDN^?P8*[D\V3KE/'/K,)RR9U@VM(1#51X':/&lt;8#"W*7.S^+&gt;G43J2V,.BE;PGUYFJ?"67FN*S;4&amp;$,O7EJ,=&gt;KJFPPO3#BV63B@_/D6V0EHBOLGFX/G_6NRXY2:-AUJ**^PRDU&gt;J6\R\J@#&lt;Z=Z8CT2))1;&gt;)C)@#.9!@A'?(`69%PV.]_T&lt;NXY2-V,Y,;C(9&amp;&lt;RP:,I*#U@=/Q97+4X#B;N&gt;N&lt;+60^X5^OZ+RZ*&amp;A2W*XQ2/5O*6MYS)ZVYF:_-XA1DK(RHJ+III'_%P@=C=!N)*MP5$-Q4(]\JB3K5EW[%E&amp;"HP?@H]W)P6@B)#$_)6@I9BIF96?$L')%/=N8M7XPH\,X%@*;I&lt;J^P&gt;]FA,]-A/YTA49D1"PG]#$EA*=`5LM4@R#&gt;+1X&gt;K#H?Y5$7[4Z&lt;%\TD3R"$5MQ%W&lt;$('D"DG;NR;DU.#]=E\O65)0=J3:.^$3YO[_1ZNQULF&amp;40%O&lt;0HZ5[SL,W'/^H+OTDC]+3QLWW.&amp;A(7*1&lt;)^\#EPBJI6\0+&lt;JYU?V.BDG@.^C'3,NM177&amp;V$?U?-UB#()4*LI;;21#KMJ;EL8&lt;W+7$R`5WG'%&gt;\D:UC&amp;&gt;[G)&gt;%HO'!*I5+W;P,15KZ48,LH:Y_+Z[=&amp;=(NN."G*+Y1&gt;`9QSC*&amp;\52GS4#VD%F=;\R@@45_#N&lt;13`)Y-;WQO`CW_R#'%1B8$GO%#[92#'%.RZ&gt;#''\61CX=X#^JB!CL-:T_S,#?F_\;Y?V&gt;5R9I[6AV;];E4UZCO+K0#SOCS94VT70A;N7".&gt;+'[[X&gt;6TH)[[(!?SY`DYGLCN,Y-IQ^&lt;;G^N-\O\@`1#'K@TQMKM^0*KIDDY(K(3OKWTCK61R6TXJ,`?,.&lt;`O.8US,H&lt;+CD7=AL=&lt;A\[N1J\478J(;K&amp;+*&lt;?*K[9RLF"!6[M5TYLX-D];PLD0L$-`U.4B?&gt;F(_0#!W`A]/"H9U!!!!!!1!!!"Q!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!"!!!!!!!!!"F!!!!&gt;8C=9W"A+"3190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT```^8+7,E_(LE'FT2%2]Y5W770)=%!'5)':I!!!!!!!!%!!!!"Q!!"7Q!!!!)!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!13!!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!"Q!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"EA!)!!!!!!!1!&amp;!!=!!!%!!-3,(P%!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!R)M?]1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!%M)!#!!!!!!!%!#!!Q`````Q!"!!!!!!%1!!!!#1!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!"2!=!!)!!!!!A!!"V*F971A6EE!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;1!]1!!!!!!!!!#&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"A^$=G6B&gt;'6E)%FU:7V*2(-!&amp;!"1!!=!!!!"!!)!!Q!%!!5!"Q!"!!A!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"EA!)!!!!!!!1!&amp;!!-!!!%!!!!!!"=!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!",S!!A!!!!!!*!#"!=!!)!!!!61!!%ER71WRB=X.-;7*S98*Z)&amp;*F:A!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!&amp;%"Q!!A!!!!#!!!(5G6B:#"731!71(!!#!!!!!)!!!B8=GFU:3"731!!%E!B$5VB;W5A:(FO97VJ9T]!6!$R!!!!!!!!!!)7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B&amp;$4&amp;.636"@17.D:8.T,G.U&lt;!!D1"9!!Q23:7&amp;E"6&gt;S;82F!V)P6Q!!"E&amp;D9W6T=Q!!%%!Q`````Q:4&gt;(*J&lt;G=!!"R!1!!"`````Q!'$U.S:7&amp;U:71A382F&lt;5F%=Q!5!&amp;!!"Q!!!!%!!A!$!!1!"1!(!!%!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!&amp;+)!#!!!!!!!%!#!!Q`````Q!"!!!!!!%O!!!!#1!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!"2!=!!)!!!!!A!!"V*F971A6EE!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;1!]1!!!!!!!!!#&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"A^$=G6B&gt;'6E)%FU:7V*2(-!-E"1!!=!!!!"!!)!!Q!%!!5!"RR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!!!"!!A!!!!!!!!!&amp;%Z*,ER7,E&amp;M&lt;#Z4&lt;X6S9W60&lt;GRZ!!!!&amp;3!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!1!$!!5!!!!"!!!!41!!!!I!!!!!A!!"!!!!!!B!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?U!!!/X?*S&gt;5EVPUU!1@9ZD/QG5NK'E+;4NFL4FVC+1/($"Y!L*5CJ&amp;#5W0Y.DLSM+*I`7G;G`=_\@Y1@18Q/T;&lt;6%J(W*(FG:H:G@??W-!T`!"FSB/J88)*W-O2LYH?##4&lt;,K8HK&lt;*?.HL$9`]`M?X9=DT@#_5+&lt;JO#W:VQ)0)/B;*Z/:A`RCQCQK!O405K/-2U/S.P$4)]VYS&amp;I%Y:Q-?!_US&lt;Q-.4[9K/*V0A*5S8A%=V:S.@,2O9D5^3Q82&gt;,=7$I00H%8HUW#3B'_QZ$\`4M=?3J&amp;-4Y#/[]*1%&gt;C,GA_0G#`ZR$`)=9(,LR=&lt;CL@Z'][&gt;&gt;U(/#TK:')9CG5EO+"5K,OU\EUK86WY@$AQ#;[)+CQ9Y[VY[TSH0MJDJVWQGEF/#Q[*!"F2$61V%A0%.&gt;&lt;K9Z25P=1^VVY%:JS?IO6`-&lt;##,!-^CINOHUA5]Q+)ZDW,=R\8EV\\^EU`S%;)NP0[@&gt;&gt;&gt;6KRKUQ.4R3NA7,6,R8594$]F7]!CLJDS,C%'@*C[B46Z)MQSST4``-S^O3@@E([1D5J;#:N%\QP&gt;*=XR0MRH7M5'QF7W3DF&gt;7,_X8S%W';3NW9*"CNY(^@;&gt;6+%A/M)K\+"-CB&lt;:"3K\B-4LU0558W_5-09&amp;9&lt;6/MCRXN\WIMFE:1_1'U6LYK!!!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!([1!!!3^!!!!)!!!(Y1!!!!!!!!!!!!!!#!!!!!U!!!%J!!!!"Z-35*/!!!!!!!!!8B-6F.3!!!!!!!!!9R36&amp;.(!!!!!!!!!;"$1V.5!!!!!!!!!&lt;2-38:J!!!!!!!!!=B$4UZ1!!!!!!!!!&gt;R544AQ!!!!!1!!!@"%2E24!!!!!!!!!BB-372T!!!!!!!!!CR735.%!!!!!A!!!E"W:8*T!!!!"!!!!HR41V.3!!!!!!!!!O"(1V"3!!!!!!!!!P2*1U^/!!!!!!!!!QBJ9WQU!!!!!!!!!RRJ9WQY!!!!!!!!!T"-37:Q!!!!!!!!!U2'5%6Y!!!!!!!!!VB'5%BC!!!!!!!!!WR'5&amp;.&amp;!!!!!!!!!Y"75%21!!!!!!!!!Z2-37*E!!!!!!!!![B#2%6Y!!!!!!!!!\R#2%BC!!!!!!!!!^"#2&amp;.&amp;!!!!!!!!!_273624!!!!!!!!!`B%6%B1!!!!!!!!"!R.65F%!!!!!!!!"#")36.5!!!!!!!!"$271V21!!!!!!!!"%B'6%&amp;#!!!!!!!!"&amp;Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$Q!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!$U!!!!!!!!!!$`````!!!!!!!!!0Q!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!!`````Q!!!!!!!!'Q!!!!!!!!!!,`````!!!!!!!!!&gt;Q!!!!!!!!!!0````]!!!!!!!!"^!!!!!!!!!!!`````Q!!!!!!!!*%!!!!!!!!!!$`````!!!!!!!!!F1!!!!!!!!!!@````]!!!!!!!!$[!!!!!!!!!!#`````Q!!!!!!!!7E!!!!!!!!!!4`````!!!!!!!!"RA!!!!!!!!!"`````]!!!!!!!!(,!!!!!!!!!!)`````Q!!!!!!!!=]!!!!!!!!!!H`````!!!!!!!!"V!!!!!!!!!!#P````]!!!!!!!!(9!!!!!!!!!!!`````Q!!!!!!!!&gt;U!!!!!!!!!!$`````!!!!!!!!"YQ!!!!!!!!!!0````]!!!!!!!!(I!!!!!!!!!!!`````Q!!!!!!!!AE!!!!!!!!!!$`````!!!!!!!!#CA!!!!!!!!!!0````]!!!!!!!!/,!!!!!!!!!!!`````Q!!!!!!!!]E!!!!!!!!!!$`````!!!!!!!!$T!!!!!!!!!!!0````]!!!!!!!!7X!!!!!!!!!!!`````Q!!!!!!!"&lt;E!!!!!!!!!!$`````!!!!!!!!&amp;OQ!!!!!!!!!!0````]!!!!!!!!7`!!!!!!!!!!!`````Q!!!!!!!"=%!!!!!!!!!!$`````!!!!!!!!&amp;X!!!!!!!!!!!0````]!!!!!!!!8?!!!!!!!!!!!`````Q!!!!!!!"TI!!!!!!!!!!$`````!!!!!!!!(0!!!!!!!!!!!0````]!!!!!!!!=_!!!!!!!!!!!`````Q!!!!!!!"UE!!!!!!!!!)$`````!!!!!!!!(RA!!!!!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!%!!%!!!!!!!!"!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!!ABA!!!!!!!!!!!!!!!!!!"!!!!!!!"!1!!!!=!)%"Q!!A!!!"6!!!34&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G!!!91(!!#!!!!!9!!!J$&gt;'QA5G6G&lt;H6N!!!51(!!#!!!!!)!!!&gt;3:7&amp;E)&amp;:*!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!!31#%.47&amp;L:3"E?7ZB&lt;7FD0Q"5!0%!!!!!!!!!!B:"9W.F=X.P=E.S:7&amp;U;7^O,GRW&lt;'FC%5.-5V6*5&amp;^"9W.F=X-O9X2M!#.!&amp;A!$"&amp;*F971&amp;6X*J&gt;'5$5C^8!!!'17.D:8.T!!#*!0("@EV"!!!!!R:"9W.F=X.P=E.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O9X2M!$2!5!!'!!!!!1!#!!-!"!!&amp;(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"A!!!!&lt;```````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!##'!!!!!!!!!!!!!!!!!!!%!!!!!!!)"!!!!#1!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!"2!=!!)!!!!!A!!"V*F971A6EE!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;1!]1!!!!!!!!!#&amp;E&amp;D9W6T=W^S1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"A^$=G6B&gt;'6E)%FU:7V*2(-!CQ$RQ:9\^!!!!!-717.D:8.T&lt;X*$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,G.U&lt;!!W1&amp;!!"Q!!!!%!!A!$!!1!"1!((5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#!!!!!=!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!8`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!##'!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!#1!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!"2!=!!)!!!!!A!!"V*F971A6EE!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;1!]1!!!!!!!!!#&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"A^$=G6B&gt;'6E)%FU:7V*2(-!CQ$RR)M?]1!!!!-7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,G.U&lt;!!W1&amp;!!"Q!!!!%!!A!$!!1!"1!((5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#!!!!!(````_!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!#&amp;#!!A!!!!!!!!!!!!%!!!!T17.D:8.T&lt;X*$=G6B&gt;'FP&lt;CZM&gt;GRJ9DJ#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="BaseAccessorScripter.ctl" Type="Class Private Data" URL="BaseAccessorScripter.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Can Script Property Accessor.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Can Script Property Accessor.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!)2R$97YA5W.S;8"U)&amp;"S&lt;X"F=H2Z)%&amp;D9W6T=W^S!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="GetVIRefs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetVIRefs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"2!=!!)!!!!!A!!"V*F971A6EE!7%"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"B#98.F17.D:8.T&lt;X*49X*J=(2F=C"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E#!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="ScriptAccessorVIs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/ScriptAccessorVIs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)T!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!=1$$`````%V:J=H2V97QA2G^M:'6S)%ZB&lt;75!'E!B&amp;%.S:7&amp;U:3"B=S"1=G^Q:8*U;76T!!!=1#%837ZD&lt;(6E:3"F=H*P=C"U:8*N;7ZB&lt;(-!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1#%=47&amp;L:3"$&lt;'&amp;T=S"5:8*N;7ZB&lt;(-A2(FO97VJ9Q!!6!$R!!!!!!!!!!)7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B&amp;$4&amp;.636"@17.D:8.T,G.U&lt;!!D1"9!!Q23:7&amp;E"6&gt;S;82F!V)P6Q!!"E&amp;D9W6T=Q!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"A!(!!A!#1!+!!M!$!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!"%A!!!")!!!!3!!!!#A!!!!A!!!!)!!!!EA!!!!!"!!U!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="CreateBaseScripter.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateBaseScripter.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#K!!!!"!"-1(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!$&amp;.D=GFQ&gt;'6S)%^V&gt;!!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!)%"Q!!A!!!"6!!!34&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G!!!?!0!!!Q!!!!%!!A)!!"!!!!E!!!!)!!!!#!!!!!!"!!-!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="GetVIItemIDs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetVIItemIDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'A!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"1J733"*&gt;'6N352T!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
	</Item>
	<Item Name="Protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="Accessors" Type="Folder">
			<Item Name="GetReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetReadVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!=!!)!!!!!A!!"V*F971A6EE!7%"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"B#98.F17.D:8.T&lt;X*49X*J=(2F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="GetWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetWriteVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteAccess.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteAccess.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;1!]1!!!!!!!!!#&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteMakeDynamic.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteMakeDynamic.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteWriteVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteReadVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!=!!)!!!!!A!!"V*F971A6EE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteCtrlRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteCtrlRef.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'$!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteClassRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteClassRef.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!',!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#"!=!!)!!!!61!!%ER71WRB=X.-;7*S98*Z)&amp;*F:A!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="GetCtrlRefnum.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetCtrlRefnum.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'$!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
		</Item>
		<Item Name="GetControlTermIdx.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetControlTermIdx.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!"1!%!!!!$5!(!!&gt;5:8*N372Y!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!!!!!!"!!)!!!!!!!!!!!!!!!!!!!!$!A!!?!!!!!!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="GetIndicatorTermIdx.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetIndicatorTermIdx.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!"1!%!!!!$5!(!!&gt;5:8*N372Y!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!!!!!!"!!)!!!!!!!!!!!!!!!!!!!!$!A!!?!!!!!!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="CreateControl.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateControl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!=!!)!!!!"A!!$UZF&gt;S"$&lt;WZU=G^M)&amp;*F:A"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="CreateControlFromReference.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateControlFromReference.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"Q!!A!!!!'!!!/4G6X)%^C;G6D&gt;#"3:79!!"J!=!!)!!!!!A!!$6:*)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!91(!!#!!!!!9!!!J4=G-A1X2M5G6G!!!71(!!#!!!!!)!!!F733"3:7:O&gt;7U!-!$Q!!9!!Q!%!!5!"A!(!!A$!!"1!!!.!Q!!#1!!!!U&amp;!!!+!!!!#A!!!!I!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="CreateIndicator.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateIndicator.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!=!!)!!!!"A!!$UZF&gt;S"$&lt;WZU=G^M)&amp;*F:A"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="GetControlLabel.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetControlLabel.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#L!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QJ-97*F&lt;#Z5:8BU!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"*4Q!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!*!$Q!!1!!Q!%!!5!"A-!!#A!!!U#!!!*!!!!#A!!!!A!!!!!!1!(!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetElementNames.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetElementNames.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!;1%!!!@````]!"1V&amp;&lt;'6N:7ZU)%ZB&lt;76T!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetMemberName.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetMemberName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````]+476N9G6S4G&amp;N:1!!7%"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"B#98.F17.D:8.T&lt;X*49X*J=(2F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetReadVITemplatePath.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetReadVITemplatePath.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-P````](6EEA5'&amp;U;!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!(%!B&amp;UFO9WRV:'5A:8*S&lt;X)A&gt;'6S&lt;7FO97RT!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!3!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetWriteVITemplatePath.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetWriteVITemplatePath.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-P````](6EEA5'&amp;U;!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!(%!B&amp;UFO9WRV:'5A:8*S&lt;X)A&gt;'6S&lt;7FO97RT!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!3!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SelectElementToRead.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/SelectElementToRead.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SelectElementToWrite.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/SelectElementToWrite.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="WireUpReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WireUpReadVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'J!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!)!!"!&amp;A!!&amp;%VV&lt;(2J:H*B&lt;75A5X2V9X1A&lt;X6U!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1(!!#!!!!!9!!!^/:8=A1W^O&gt;(*P&lt;#"3:79!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="WireUpWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WireUpWriteVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'J!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!)!!"!&amp;A!!&amp;%VV&lt;(2J:H*B&lt;75A5X2V9X1A&lt;X6U!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1(!!#!!!!!9!!!^/:8=A1W^O&gt;(*P&lt;#"3:79!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="CallUserScripting.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CallUserScripting.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="Get or Create Folder.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Get or Create Folder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'^!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!=!!)!!!!3A!!%5:P&lt;'2F=C"3:7:O&gt;7UA&lt;X6U!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"B!)2*$=G6B&gt;'6"=V"S&lt;X"F=H2J:8-!!"R!-0````]46GFS&gt;(6B&lt;#"'&lt;WRE:8)A4G&amp;N:1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!2)!!!#3!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="WouldCrowdProperty.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WouldCrowdProperty.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%H!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1R8&lt;X6M:#"D=G^X:$]!!"Z!=!!)!!!!3A!!%5:P&lt;'2F=C"3:7:O&gt;7UA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)%F0!!!11#%,6W&amp;O&gt;(-A6X*J&gt;'5!$E!B#6&gt;B&lt;H2T5G6B:!!G1(!!#!!!!%I!!"F1=G^Q&lt;X.F:#"'&lt;WRE:8)A5G6G&lt;H6N)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#1!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%A!!!"!!!!!1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Templates" Type="Folder">
			<Item Name="WriteTemplate_NoErrorTerminals.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteTemplate_NoErrorTerminals.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$,!!!!"!!%!!!!-%"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!72%Z-8URB9F:*26=A4W*K:7.U)'^V&gt;!!!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!#!A!!?!!!!!!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082917376</Property>
			</Item>
			<Item Name="ReadTemplate_NoErrorTerminals.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/ReadTemplate_NoErrorTerminals.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$,!!!!"!!%!!!!-%"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!72%Z-8URB9F:*26=A4W*K:7.U)'^V&gt;!!!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!#!A!!?!!!!!!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082917376</Property>
			</Item>
			<Item Name="ReadTemplate.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/ReadTemplate.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!&amp;E2/4&amp;^-97*73568)%^C;G6D&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1343103488</Property>
			</Item>
			<Item Name="WriteTemplate.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteTemplate.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!&amp;E2/4&amp;^-97*73568)%^C;G6D&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">11</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082393088</Property>
			</Item>
		</Item>
		<Item Name="AddControlToWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddControlToWriteVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="AddIndicatorToReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddIndicatorToReadVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="AddVIsToClass.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddVIsToClass.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#"!=!!)!!!!3A!!%E2F=X2J&lt;G&amp;U;7^O)%:P&lt;'2F=A!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="AddVIToClass.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddVIToClass.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(H!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)E"Q!!A!!!"+!!!54G6X)("S&lt;WJF9X1A;82F&lt;3"S:79!!"J!=!!)!!!!!A!!$8:J)(*F:GZV&lt;3"P&gt;81!*E"Q!!A!!!"6!!!:4&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G&lt;H6N)'^V&gt;!!%!!!!)%"Q!!A!!!"+!!!32'6T&gt;'FO982J&lt;WYA2G^M:'6S!!"5!0%!!!!!!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%5.-5V6*5&amp;^"9W.F=X-O9X2M!#.!&amp;A!$"&amp;*F971&amp;6X*J&gt;'5$5C^8!!!'17.D:8.T!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]15(*P='^T:71A6EEA4G&amp;N:1!!'E"Q!!A!!!!#!!!-&gt;GEA=G6G&lt;H6N)'FO!!!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!A!#1!+!!M!$!!.!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!%A!!!!A!!!!+!!!"#A!!!!A!!!!+!!!!!!%!$A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="CosmeticCleanup.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CosmeticCleanup.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="GetDistinctName.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetDistinctName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].17.D:8"U:71A&lt;G&amp;N:1!G1(!!#!!!!&amp;5!!"F-6E.M98.T4'FC=G&amp;S?3"3:7:O&gt;7UA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!Q`````QV1=G^Q&lt;X.F:#"O97VF!#*!=!!)!!!!61!!&amp;5R71WRB=X.-;7*S98*Z)&amp;*F:GZV&lt;1"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!%+!!!!#A!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="InstantiateAccessorVIs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/InstantiateAccessorVIs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!=1#%837ZD&lt;(6E:3"F=H*P=C"U:8*N;7ZB&lt;(-!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"A!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!")!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SetUpClassControls.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/SetUpClassControls.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;*!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!35W.S;8"U:8)A1WRB=X-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!=!!)!!!!!A!!$&amp;:*)&amp;*F:G6S:7ZD:1!!4E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!!Z49X*J=(2F=C"$&lt;'&amp;T=Q!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="DeleteSeqStruct.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/DeleteSeqStruct.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"U!!!!!Q!%!!!!)%"Q!!A!!%!7!!!4486M&gt;'FG=G&amp;N:3"4&gt;(6D&gt;#"J&lt;A")!0!!#A!!!!!!!!!!!!!!!!!!!!!!!!!"!Q!!U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%A!!!!!"!!)!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082655248</Property>
		</Item>
		<Item Name="Get Property Folder if Exists.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Get Property Folder if Exists.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!"!!!!#J!=!!)!!!!3A!!(6"B=G6O&gt;#"1=G^K:7.U382F&lt;3"3:7:O&gt;7UA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(E!Q`````R21=G^Q:8*U?3"'&lt;WRE:8)A4G&amp;N:1!!+E"Q!!A!!!"+!!!=5'&amp;S:7ZU)&amp;"S&lt;WJF9X2*&gt;'6N)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!&amp;!!5!"1!&amp;!!=!#!!&amp;!!E$!!"Y!!!.#!!!#1!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!%3!!!!!!!!!"!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Add or Get Property Folder.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Add or Get Property Folder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!$E!B#%.S:7&amp;U:71`!!!%!!!!(E!Q`````R21=G^Q:8*U?3"'&lt;WRE:8)A4G&amp;N:1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11#%+6W&amp;O&gt;(.8=GFU:1!!$E!B#6&gt;B&lt;H2T5G6B:!!K1(!!#!!!!%I!!"R198*F&lt;H1A5(*P;G6D&gt;%FU:7UA5G6G&lt;H6N)'FO!!"5!0!!$!!$!!1!"1!'!!9!"A!(!!9!#!!*!!I!#Q-!!(A!!!U)!!!.#Q!!#1!!!!!!!!!!!!!!!!!!!AA!!!!!!!!!#A!!!!I!!!!+!!!!#A!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Get Virtual Folder if Exists.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Get Virtual Folder if Exists.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!"!!!!#:!=!!)!!!!61!!'5R71WRB=X.-;7*S98*Z)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1$$`````%V:J=H2V97QA2G^M:'6S)%ZB&lt;75!*E"Q!!A!!!"6!!!94&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G&lt;H6N)'FO!!"5!0!!$!!$!!1!"1!'!!5!"1!&amp;!!5!"Q!)!!5!#1-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!2)!!!!!!!!!%!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Add or Get Virtual Folder.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Add or Get Virtual Folder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!"!!!!#:!=!!)!!!!61!!'5R71WRB=X.-;7*S98*Z)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1$$`````%V:J=H2V97QA2G^M:'6S)%ZB&lt;75!*E"Q!!A!!!"6!!!94&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G&lt;H6N)'FO!!"5!0!!$!!$!!1!"1!'!!5!"1!&amp;!!5!"Q!)!!5!#1-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!2)!!!!!!!!!%!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
</LVClass>
