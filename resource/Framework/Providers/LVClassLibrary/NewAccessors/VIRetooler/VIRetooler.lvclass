﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="" Type="Bool">true</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">MemberVICreation.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/MemberVICreation.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\&gt;7^=&gt;N!%)&lt;BTRY%4BE[]X"'&amp;7Q$VAC*#W!,GSN#KEA$F&lt;!&amp;+'%,&lt;'&amp;&lt;9!NM!8LPM/:1A=6%]HA]/OAA],O`2Q=1EHLZ*AV;LJ8BT&lt;+WNX.&gt;,%/`8I:V[(J;E^^^KHW^OIT0%`VJ`$!-S]8YV_MPFW.?N\`FPV,U74\,`VU?LHY*0PR\]%8N252.;F#&gt;;GL,,EG?Z%G?Z%G?Z%%?Z%%?Z%%?Z%\OZ%\OZ%\OZ%:OZ%:OZ%:OZ0UA&amp;\H)21YJ74R:+*EUG3$J$%8*+@%EHM34?0CIR*.Y%E`C34RU5?**0)EH]31?BCHR**\%EXA3$V.V3@;$(%`C98I&amp;HM!4?!*0Y'&amp;*":Y!%#Q74"R-!E."9X!2?!*0Y/&amp;3A3@Q"*\!%XBI6O!*0)%H]!1?BP2&gt;C;ZJ"TE?JJ(D=4S/R`%Y(K;7YX%]DM@R/"[7E_.R0!\#7&gt;#:()+=15Y(ZY0D=4T]EO.R0)\(]4A?GPI&gt;]LYT4&gt;-/=DS'R`!9(M.D?*B#BM@Q'"\$9XC96I&lt;(]"A?QW.Y7%K'R`!9(A.C,-LS-C9T"BK&gt;D-$Q]./@&amp;ONX+&lt;L%_C(6Q[N[+&amp;50G_IB5DU=KJOOOJGKG[4;@.7GKD:,N1GK0U[&amp;6G&amp;5C[A'NYY[=4Z3$^1^&gt;;:/V"VVJ'[JGT&lt;UH4O?4C=&gt;DU=&gt;$A@N^XP.][RJGL4&lt;\43/I\&lt;&lt;L4;&lt;T@EV])0D`%*9XUM08$`?0&gt;]`D4?`ZLO&lt;_[?@T\?0U`@&lt;FP_&amp;`]``Q,N28X7Z"HPU!IXZ10A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.3</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"C@5F.31QU+!!.-6E.$4%*76Q!!&amp;#A!!!2X!!!!)!!!&amp;!A!!!!O!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!!!!!!I#!!A!!!-!!!+!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"PA;0^0]V`3K,PP$YK:ZOU!!!!$!!!!"!!!!!!"6(NM-A)\E#R@-,$-\E!_N1&gt;D.G0!,)%[9!*G/TY1HY!!!!!!!!!!"S^E/^4C0Z#B(MGD2Z2'F]"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1BLSBNX8WILH._;FP6BQBZQ!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#=!!!!K?*RDY':A;G#YQ!$%D%$-V-$U!]D_!/)T#("!3!A.!0!+#S-!!!!!3!!!!2BYH'.AQ!4`A1")-4)Q-(U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"R5D1B%DOE/%*^!.Y=.C^E!J/5I:1!!!!Q!!6:*2&amp;-!!!!!!!-!!!($!!!%+(C=3W"C9-AUND!\!+3:'2E9&amp;"A;'*,T5V+Z')"]"AA19''A'!2!T&gt;.#%T=]=$A.#04YZ6P!`'Y8&amp;:&lt;G'B5?JF+_`S5K(!%P1),.2TA/&gt;XPE((?U!3PBS',)9ADY(ZD2@)1(L"N:PY]+C_'"BEJFBF,BYYUGD""&lt;!K('M"Q/[Q;;#L2%"+B1I,00"OCGTD9()0H@Y,]BT,,/AY9(GH]S]E]ZS.`["'2I'UB&gt;LQ=,2%.X(UB$&lt;S",ZT3QTE+7`Q5MW03O)6NP;17:_FR)U7&gt;YMPE(3&amp;@HC&lt;D$(7C98\Y"&amp;B`!I/)Y_*#F&gt;S*9I".%&gt;I9Q3BRX9&gt;12!\*\?2A$%@'%&amp;B`A]-ZC$)0*&gt;\-&gt;&gt;^!!M9]\C%#I$!B6!;%+1.1/%"&amp;XG*BUN@&lt;VP6WM1*I.3=Q"CE%?!+6L'.:D9'1!*1AG)!4:_O@```]W1"%GK*AC6!T%0ME)94-T-I,4,5CM$UH_&amp;"+\$+LW*:*:&lt;)Q)?4MI7Q0*`J&gt;1-1=E.Y0UAER9#Z44A,)X1^E.5$_#R,Y#R1KA\$_-E,Q&amp;9D-$$6D!#'&amp;T!&gt;E#5(&amp;")0M"6&amp;Q-SA;&amp;"Q-$*OXM\_++(,[QP!]!EF4CU!!!!!!/)!'!!!!!"D)Q,D!O-1!!!!!!!!QA!)!!!!!%-D!O-!!!!!!/)!'!!!!!"D)Q,D!O-1!!!!!!!!QA!)!!!!!%-D!O-!!!!!!/)!'!!!!!"D)Q,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!5&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!7N79/N"1!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!7N73]P,S_$L15!!!!!!!!!!!!!!!!!!!!!``]!!!7N73]P,S]P,S]PA[U&amp;!!!!!!!!!!!!!!!!!!$``Q#$73]P,S]P,S]P,S]P,Y/N!!!!!!!!!!!!!!!!!0``!&amp;F:,S]P,S]P,S]P,S]P`I-!!!!!!!!!!!!!!!!!``]!79/$73]P,S]P,S]P`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AVEP,S]P`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY.:L@\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!)/$AY/$AY/$`P\_`P\_AY-!!!!!!!!!!!!!!!!!``]!!&amp;F:AY/$AY0_`P\_A[V:!!!!!!!!!!!!!!!!!!$``Q!!!!":AY/$A`\_AY.:!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!79/$AY-P!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!&amp;EP!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!!"!!!!!!!!!0W!!!,YHC=L::,4".2&amp;)&lt;0(6O=AI]\_++*J#-:K#_-EKA)CCC$7F3+)M;XD(1%&amp;-'56FU9W41GR"!82":'&amp;];%L1M7OD%RJH%T'V=O.,%B=7GC'[)*4M=TNZVJBT[5U#ZO#LX`_=^]ZZ_&lt;#V$RF6:R38CA![&amp;T_/7%$O5BD1$%[XF)@_I?!OUB]U$7?IE/L8Q0`=)FS59&gt;6I3U/H[H0!Y`=&lt;@RQP#4(?1KH=7N:&gt;3,R=JV7"X3.AA&gt;5I*+LT:+YW[LKA$6&gt;))EO8/3\R=`%2N"1YBN.F?BHC3"S*N=LFB.FT+MRC4TPZZ[XMN+?H3AMF9&gt;FB*_L)D7\VF*TE`O=YV73=#3GW&amp;G:C9D%F+C/N:')WL)@1!SQ@G,;#J2-SIFND../&gt;/ATY4F)`O3EW&lt;PJGCB&gt;)WM5:3C\F\[E5WNJ&gt;.&lt;]_I'&gt;&amp;AL*1\T8HYW^.G^]F4M.2!A];O]]&gt;+Y3^ZS@K(4H!,&lt;7YG$E(&amp;/N5W%&gt;O0@X4JMD7F=,\AMPQ'9:+.Q7;09&lt;Y\C#"O&amp;2R[HE*H&amp;Z$^G)&lt;PC.&gt;6N1^(2C"I72[[,@50+[+BY/TRY2YGI9EC*+,F4/C"LXLUG!&gt;/-"147A9O%Y(%W]2'9HJZ'#,BGJ#UIX3!F&lt;*W1@BK,?CB$TX4.U$O)^/4@:`J.AL6.H*6&lt;.]PN2&lt;:O9_N*NBZC[ZF-HBMQTQ*&gt;W+YDT\N,H_=^',-(#`)-DUA1HB8*ZN[5+#P01;45#Y_+;"J2-_&lt;)=R"^?CW@9HH?FZPHI/6FZXFK;GJBHJPM0,M)3?5Z^N(Y9`QR5TVD@#.09:KFWMU504A/7&gt;P*IDG0(CXI-7P`CPD\]W;9)0^--U`S$E!MAV31P5?CE7B9&amp;&lt;O5M$I=%&lt;P#)T@5PEAAIN\+(=W!L&amp;8NEB,&gt;['9[C=""+^151FSBQW!K^8:UVW$".J#SI1I_@##/?N0)PZPCV(?$]F[0,_._!YMV/V_B+H"$#`CM:T7%1A@*412@?RZK,Y$]?_76V!OR$&amp;_)^!Y%@2*(SPB&amp;M==XU/Q%@;YA[/&lt;C12?Z./?+9(CQ@X"9'2,0"H,*HH?3^3(:AU8*8CAFW5N,)(MZ$VG8A_QWCWQ$^BA&amp;V5F7,EB7`6?%,&lt;4#;@8WE.+HXD)$T!\D8-$NCQ8=75L!Q35!\MI'0*Q#\(9!PJA'&lt;0T!(BOAQQF9+1CYYX]"L\+T7Y$ON=83\3MFX&gt;!3[+JZ[*9Z["[SYPM'?ZS(,5[[2QP3X@+@"U.:JXIX\ZFQ&lt;,&amp;1![7%WL%%K-@TH!H,'620:V&lt;``*=05@N7T8&gt;,MF&amp;M!WX(YG-[L*0&lt;+N`2A(HVYQ.]/XW/FR7]N[THH`.T]5`WP4N_QF&lt;'0M,D:?OFJT[_[C]2;V#^!!!!!!!%!!!!3!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!!1!!!!!!!!!:1!!!(6YH'.A9#A5E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````6SFCZ0B[Z"J=U2%@/&amp;.FFDS("!"F#"G;!!!!!!!!"!!!!!=!!!1\!!!!#!!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!$5)!#!!!!!!!%!#!!Q`````Q!"!!!!!!#Y!!!!"A!=1(!!#!!!!&amp;5!!!Z0=GFH;7ZB&lt;#"D&lt;'&amp;T=Q!!(E"Q!!A!!!"6!!!25G6Q&lt;'&amp;D:7VF&lt;H1A9WRB=X-!'%"Q!!A!!!!#!!!,4X*J:WFO97QA6EE!&amp;%"Q!!A!!!!#!!!'4G6X)&amp;:*!!!G1(!!#!!!!%I!!"F'&gt;82V=G5A5'&amp;S:7ZU)&amp;"S&lt;WJF9X2*&gt;'6N!#2!5!!&amp;!!!!!1!#!!-!""*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!!!%!"1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!Z)!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!&amp;!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"EA!)!!!!!!!1!&amp;!!=!!!%!!-A^5?A!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!S$V2[!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!$5)!#!!!!!!!%!#!!Q`````Q!"!!!!!!#Y!!!!"A!=1(!!#!!!!&amp;5!!!Z0=GFH;7ZB&lt;#"D&lt;'&amp;T=Q!!(E"Q!!A!!!"6!!!25G6Q&lt;'&amp;D:7VF&lt;H1A9WRB=X-!'%"Q!!A!!!!#!!!,4X*J:WFO97QA6EE!&amp;%"Q!!A!!!!#!!!'4G6X)&amp;:*!!!G1(!!#!!!!%I!!"F'&gt;82V=G5A5'&amp;S:7ZU)&amp;"S&lt;WJF9X2*&gt;'6N!#2!5!!&amp;!!!!!1!#!!-!""*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!!!%!"1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'3!!A!!!!!!"!!5!!Q!!!1!!!!!!&amp;!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!$5)!#!!!!!!!9!(%"Q!!A!!!"6!!!/4X*J:WFO97QA9WRB=X-!!"Z!=!!)!!!!61!!%6*F='RB9W6N:7ZU)'.M98.T!"B!=!!)!!!!!A!!#U^S;7&gt;J&lt;G&amp;M)&amp;:*!"2!=!!)!!!!!A!!"EZF&gt;S"731!!*E"Q!!A!!!"+!!!:2H6U&gt;8*F)&amp;"B=G6O&gt;#"1=G^K:7.U382F&lt;1!E1&amp;!!"1!!!!%!!A!$!!136EF3:82P&lt;WRF=CZM&gt;G.M98.T!!!"!!5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!54EEO4&amp;9O17RM,F.P&gt;8*D:5^O&lt;(E!!!!6)!#!!!!!!!%!"!!B!!%!!!%!!!!!!!!!!!1!"Q!.!!!!"!!!!'M!!!!I!!!!!A!!"!!!!!!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!1!!!!!!!!!!!!!!!!!!!!!:U")!!!!!!!."!F66"5%!4A!*!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+OK+D[`7()_K[IK0L^9=DY!!!!!!!."!A!!!$]!!!!!!!!!!!!!!6]!!!*9?*S65%V0QE!5(&amp;A_":%P%2".4RYZ[.GE#1E**GIFE&lt;/F,+2GI73\I%&gt;`HD`&amp;I`Y$JV$"R*.Z;@P?T/TM^!(II'-PE10Q#*4ON4`T&amp;[[S0/7')8#_ZSJ$O63O*_&gt;S97+['&lt;.*I,!\/2KAPM=T&gt;`)FAH!29T&gt;!K\]S+SUNR^72F[/$:_G:A:&amp;TB0B[PX\YI%YU&lt;O6],06IU.03.8[Q[+KV]M@6U7!I42!IK1FM=J2_1:Z2O,1&gt;J'G29!#"V&amp;F0L5)DN26-N\GNJ@&lt;8LJ(7R$5OF&gt;1?9%,^*T)=2$TCCI(T&gt;B:CKG&lt;)W7]C'*IN))-JSLR&amp;5&amp;F!5;QG5R3R7^7O4`\K_?.=4*3MR$JCF96ZH&gt;$"I?Y1&amp;84`G:PO;3&lt;G+Q&amp;?]=3D+@2J:['/9T*2.=D]6#;OP]C?/&gt;H5&gt;A%*..FZ'^]%([$'S\,)I]KOB4;2.G/E=-KZRG]SCA,R$4$09^9!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!&amp;#A!!!2X!!!!)!!!&amp;!A!!!!!!!!!!!!!!#!!!!!U!!!%;!!!!"Z-35*/!!!!!!!!!8B-6F.3!!!!!!!!!9R36&amp;.(!!!!!!!!!;"$1V.5!!!!!!!!!&lt;2-38:J!!!!!!!!!=B$4UZ1!!!!!!!!!&gt;R544AQ!!!!!!!!!@"%2E24!!!!!!!!!A2-372T!!!!!!!!!BB735.%!!!!!!!!!CRW:8*T!!!!"!!!!E"41V.3!!!!!!!!!K2(1V"3!!!!!!!!!LB*1U^/!!!!!!!!!MRJ9WQY!!!!!!!!!O"-37:Q!!!!!!!!!P2'5%6Y!!!!!!!!!QB'5%BC!!!!!!!!!RR'5&amp;.&amp;!!!!!!!!!T"75%21!!!!!!!!!U2-37*E!!!!!!!!!VB#2%6Y!!!!!!!!!WR#2%BC!!!!!!!!!Y"#2&amp;.&amp;!!!!!!!!!Z273624!!!!!!!!![B%6%B1!!!!!!!!!\R.65F%!!!!!!!!!^")36.5!!!!!!!!!_215F1A!!!!!!!!!`B71V21!!!!!!!!"!R'6%&amp;#!!!!!!!!"#!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$M!!!!!!!!!!$`````!!!!!!!!!01!!!!!!!!!!0````]!!!!!!!!")!!!!!!!!!!!`````Q!!!!!!!!%I!!!!!!!!!!$`````!!!!!!!!!61!!!!!!!!!!0````]!!!!!!!!"I!!!!!!!!!!!`````Q!!!!!!!!'Q!!!!!!!!!!4`````!!!!!!!!!XA!!!!!!!!!"`````]!!!!!!!!$D!!!!!!!!!!)`````Q!!!!!!!!/=!!!!!!!!!!H`````!!!!!!!!!\!!!!!!!!!!#P````]!!!!!!!!$Q!!!!!!!!!!!`````Q!!!!!!!!05!!!!!!!!!!$`````!!!!!!!!!_Q!!!!!!!!!!0````]!!!!!!!!%!!!!!!!!!!!!`````Q!!!!!!!!3%!!!!!!!!!!$`````!!!!!!!!#)A!!!!!!!!!!0````]!!!!!!!!)G!!!!!!!!!!!`````Q!!!!!!!!CA!!!!!!!!!!$`````!!!!!!!!$*Q!!!!!!!!!!0````]!!!!!!!!-J!!!!!!!!!!!`````Q!!!!!!!!SM!!!!!!!!!!$`````!!!!!!!!$,Q!!!!!!!!!!0````]!!!!!!!!-R!!!!!!!!!!!`````Q!!!!!!!!UQ!!!!!!!!!!$`````!!!!!!!!$4A!!!!!!!!!!0````]!!!!!!!!2?!!!!!!!!!!!`````Q!!!!!!!"'!!!!!!!!!!!$`````!!!!!!!!%9A!!!!!!!!!!0````]!!!!!!!!2N!!!!!!!!!!!`````Q!!!!!!!")Y!!!!!!!!!)$`````!!!!!!!!%ZQ!!!!!$F:*5G6U&lt;W^M:8)O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!#!!"!!!!!!!!!1!!!!%!"A"1!!!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!)5)!#!!!!!!!!!!!!!!!!!1!!!!!!!1%!!!!%!"J!=!!)!!!!!A!!$6"B=G6O&gt;#".:82I&lt;W1!'%"Q!!A!!!"6!!!+4X*J:S"D&lt;'&amp;T=Q!!(E"Q!!A!!!"6!!!25G6Q&lt;'&amp;D:7VF&lt;H1A9WRB=X-!&lt;Q$RQSLB]!!!!!-717.D:8.T&lt;X*$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-/6EF3:82P&lt;WRF=CZD&gt;'Q!,E"1!!-!!!!"!!)&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!$!!!!!````````````````Q!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!!B1A!)!!!!!!!!!!!!!!!!"!!!!!!!#!1!!!!-!&amp;E"Q!!A!!!!#!!!*476N9G6S)&amp;:*!"B!=!!)!!!!61!!#E^S;7=A9WRB=X-!!'U!]=-KYQ1!!!!$&amp;E&amp;D9W6T=W^S1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T$F:*5G6U&lt;W^M:8)O9X2M!#R!5!!#!!!!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!#!!!!!!!!!!%!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!!B1A!)!!!!!!!!!!!!!!!!"!!!!!!!$!1!!!!1!&amp;E"Q!!A!!!!#!!!*476N9G6S)&amp;:*!"B!=!!)!!!!61!!#E^S;7=A9WRB=X-!!"Z!=!!)!!!!61!!%6*F='RB9W6N:7ZU)'.M98.T!']!]=-KYQA!!!!$&amp;E&amp;D9W6T=W^S1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T$F:*5G6U&lt;W^M:8)O9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!-!!!!!!!!!!@````]!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!)5)!#!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!%!":!=!!)!!!!!A!!#5VF&lt;7*F=C"731!91(!!#!!!!&amp;5!!!J0=GFH)'.M98.T!!!?1(!!#!!!!&amp;5!!"&amp;3:8"M97.F&lt;76O&gt;#"D&lt;'&amp;T=Q"P!0($+O-)!!!!!R:"9W.F=X.P=E.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=QZ736*F&gt;'^P&lt;'6S,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!"`````A!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!!B1A!)!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!5!&amp;%"Q!!A!!!!#!!!'4G6X)&amp;:*!!!91(!!#!!!!&amp;5!!!J0=GFH)'.M98.T!!!?1(!!#!!!!&amp;5!!"&amp;3:8"M97.F&lt;76O&gt;#"D&lt;'&amp;T=Q!91(!!#!!!!!)!!!N0=GFH;7ZB&lt;#"731"R!0($-#^Z!!!!!R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=QZ736*F&gt;'^P&lt;'6S,G.U&lt;!!Q1&amp;!!"!!!!!%!!A!$(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"!!!!!1!!!!!!!!!!1!!!!,`````!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!!B1A!)!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!5!(%"Q!!A!!!"6!!!/4X*J:WFO97QA9WRB=X-!!"Z!=!!)!!!!61!!%6*F='RB9W6N:7ZU)'.M98.T!"B!=!!)!!!!!A!!#U^S;7&gt;J&lt;G&amp;M)&amp;:*!"2!=!!)!!!!!A!!"EZF&gt;S"731!!=1$RQTQD]Q!!!!-7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-/6EF3:82P&lt;WRF=CZD&gt;'Q!-%"1!!1!!!!"!!)!!RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!1!!!!%!!!!!1!!!!)!!!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!)5)!#!!!!!!!!!!!!!!!!!1!!!!!!!Q!!!!!'!"R!=!!)!!!!61!!$E^S;7&gt;J&lt;G&amp;M)'.M98.T!!!?1(!!#!!!!&amp;5!!"&amp;3:8"M97.F&lt;76O&gt;#"D&lt;'&amp;T=Q!91(!!#!!!!!)!!!N0=GFH;7ZB&lt;#"731!51(!!#!!!!!)!!!:/:8=A6EE!!#:!=!!)!!!!3A!!'5:V&gt;(6S:3"198*F&lt;H1A5(*P;G6D&gt;%FU:7U!=Q$RS$V2[!!!!!-7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-/6EF3:82P&lt;WRF=CZD&gt;'Q!-E"1!!5!!!!"!!)!!Q!%(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"1!!!!5!!!!!!!!!!1!!!!)!!!!$`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!%!#!!!!!!!!!!!!!!!%!!!!J17.D:8.T&lt;X*$=G6B&gt;'FP&lt;CZM&gt;GRJ9DJ736*F&gt;'^P&lt;'6S,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.3</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="VIRetooler.ctl" Type="Class Private Data" URL="VIRetooler.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="CLSUIP_ResizeBDWin.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_ResizeBDWin.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%P!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!=!!)!!!!!A!!$6:*)&amp;*F:GZV&lt;3"P&gt;81!1E"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$6:*5G6U&lt;W^M:8)A;7Y!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71(!!#!!!!!)!!!F733"3:7:O&gt;7U!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!'!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!E!!!!!I!!!!!!!!!!!!!!"!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_RetoolVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_RetoolVI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%E!Q`````QF/:8&gt;*&gt;'6N351!"!!!!%2!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!Z736*F&gt;'^P&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!+6EF3:82P&lt;WRF=A!!91$Q!!Q!!Q!%!!5!"A!&amp;!!5!"1!&amp;!!=!"1!&amp;!!A$!!"Y!!!*!!!!#1!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!I!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_InitVIRetooler.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_InitVIRetooler.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!Z736*F&gt;'^P&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!?1(!!#!!!!&amp;5!!"&amp;3:8"M97.F&lt;76O&gt;#"D&lt;'&amp;T=Q!71(!!#!!!!!)!!!F.:7VC:8)A6EE!1E"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$6:*5G6U&lt;W^M:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_AddNewMethodToChild.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_AddNewMethodToChild.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%*!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!=!!)!!!!3A!!%5&amp;E:%FU:7V'=G^N476N&lt;X*Z!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!#F:*5G6U&lt;W^M:8)!!%)!]!!*!!-!"!!&amp;!!1!"!!'!!1!"!!(!Q!!;!!!$15!!!!!!!!*!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_CenterOnPoint.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_CenterOnPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!(1!-!!8A!"U!$!!&amp;Z!"2!5!!#!!5!"AB1&lt;X.J&gt;'FP&lt;A!!(%"Q!!A!!%!,!!!01X2S&lt;&amp;2F=GUA5G6G&lt;H6N!#1!]!!%!!-!"!!(!!A$!!!A!!!.!1!!#A!!!!A!!!!)!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
	</Item>
	<Item Name="CLSUIP_CreateNewVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_CreateNewVI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#1"!1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!+6EF3:82P&lt;WRF=A!!"!!!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!)!!Q!%%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$F:*5G6U&lt;W^M:8)A&lt;X6U!!!71&amp;!!!Q!#!!-!"!FF=H*P=C"P&gt;81!5Q$Q!!I!!!!"!!%!"1!"!!%!"A!"!!%!"Q-!!()!!!I!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!U!!!!!!!!!!!!!!!U$!!M!!!!!!!!"!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_DropAndWireNewDiag.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_DropAndWireNewDiag.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!=!!)!!!!!A!!$6:*)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!+6EF3:82P&lt;WRF=A!!0!$Q!!A!!Q!%!!1!"1!'!!1!"!!(!Q!!9!!!$11!!!!!!!!!!!!!#1!!!!I!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1073742352</Property>
	</Item>
	<Item Name="CLSUIP_GetContainingLVClass.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_GetContainingLVClass.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!=!!)!!!!61!!%E.P&lt;H2B;7ZJ&lt;G=A4&amp;:$&lt;'&amp;T=Q!!'E"Q!!A!!!!#!!!.476N9G6S)&amp;:*)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!=!!)!!!!!A!!#5VF&lt;7*F=C"731"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_GetFPTermFromTerminal.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_GetFPTermFromTerminal.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#^!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"Q!!A!!%!,!!!01X2S&lt;&amp;2F=GUA5G6G&lt;H6N!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'%"Q!!A!!%!"!!!,6'6S&lt;3"3:7:O&gt;7U!*!$Q!!1!!Q!%!!5!"A-!!#A!!!U#!!!*!!!!#A!!!!A!!!!!!1!(!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_GetProjItemOfMemberVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_GetProjItemOfMemberVI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%"Q!!A!!!"+!!!,5(*P;G6D&gt;%FU:7U!&amp;E"Q!!A!!!"6!!!*1WRB=X-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;%!Q`````QN.:7VC:8)A4G&amp;N:1!71(!!#!!!!&amp;5!!!B$&lt;'&amp;T=S"J&lt;A!!-!$Q!!9!!Q!%!!5!"A!(!!A$!!"1!!!.!Q!!#1!!!!U&amp;!!!+!!!##!!!!!I!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
	</Item>
	<Item Name="CLSUIP_GetUnlockPrompt.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_GetUnlockPrompt.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"]!!!!!Q!=1$$`````%GRP9W&amp;M;8JF:#"Q=G^N=(1A-A!!1%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!#F:*5G6U&lt;W^M:8)!!"A!]!!#!!!!!1)!!!A!!!E!!!#1!!!!!!%!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_GrowRightDirection.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_GrowRightDirection.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7!!!!"Q!01!)!#7ZF&gt;S"M&lt;X&gt;F=A!21!)!#GZF&gt;S"I;7&gt;I:8)!!"N!!Q!6486M&gt;'FQ&lt;'FD982J&lt;WYA2G&amp;D&gt;'^S!!N!!A!&amp;&lt;'^X:8)!$5!#!!:I;7&gt;I:8)!!!N!!Q!&amp;='^J&lt;H1!-!$Q!!9!!!!"!!)!!Q!%!!5#!!%!!!!*!!!!#1!!!!I!!!!)!!!!#A!!!!I!!!!!!1!'!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
	</Item>
	<Item Name="CLSUIP_GrowTermBounds.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_GrowTermBounds.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$9!!!!#Q!,1!)!"'RF:H1!!!F!!A!$&gt;'^Q!!N!!A!&amp;=GFH;(1!$5!#!!:C&lt;X2U&lt;WU!!"R!5!!%!!!!!1!#!!--2X*P&gt;WYA1G^V&lt;G2T!!!(1!-!!8A!"U!$!!&amp;Z!"B!5!!#!!5!"AR$:7ZU:8)A5'^J&lt;H1!!#"!5!!#!!5!"B6.&gt;7RU;8"M;7.B&gt;'FP&lt;C"'97.U&lt;X)!(E"1!!1!!!!"!!)!!Q^5:8*N;7ZB&lt;#"#&lt;X6O:(-!*!$Q!!1!"!!(!!A!#1)!!#!!!!U$!!!)!!!!#!!!!!I!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_OverrideAuthentication.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_OverrideAuthentication.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%.!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J$97ZD:7RM:71`!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!J736*F&gt;'^P&lt;'6S!!"5!0!!$!!$!!1!"1!%!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073774912</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
	</Item>
	<Item Name="CLSUIP_OverrideErrMsgs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_OverrideErrMsgs.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!Q!!!!!A!A1$$`````&amp;E.P&lt;G:M;7.U;7ZH)%ZB&lt;75A28*S&lt;X)!!!A!5!!"!!!!!1!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_ReplaceClassCtls.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_ReplaceClassCtls.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%S!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!Z736*F&gt;'^P&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!+6EF3:82P&lt;WRF=A!!21$Q!!A!!Q!%!!1!"1!'!!1!"!!(!Q!!9!!!#1!!!!!!!!!!!!!!$1=!!!I!!!!!!!!!!!!!!!I!!!E!!!!)!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_Read New VI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_Read New VI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"_!!!!!Q!51(!!#!!!!!)!!!:/:8=A6EE!!%J!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!"6736*F&gt;'^P&lt;'6S,GRW9WRB=X-A;7Y!'!$Q!!)!!!!"!A!!#!!!#1!!!"!!!!!!!1!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_Read Original VI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_Read Original VI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!##!!!!!Q!91(!!#!!!!!)!!!N0=GFH;7ZB&lt;#"731"+1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!66EF3:82P&lt;WRF=CZM&gt;G.M98.T)'FO!"A!]!!#!!!!!1)!!!A!!!E!!!!1!!!!!!%!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_Read Original class.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_Read Original class.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!##!!!!!Q!91(!!#!!!!&amp;5!!!J0=GFH)'.M98.T!!"+1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!66EF3:82P&lt;WRF=CZM&gt;G.M98.T)'FO!"A!]!!#!!!!!1)!!!A!!!E!!!!1!!!!!!%!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_Read Replacement class.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_Read Replacement class.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#)!!!!!Q!?1(!!#!!!!&amp;5!!"&amp;3:8"M97.F&lt;76O&gt;#"D&lt;'&amp;T=Q"+1(!!(A!!+R:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%F:*5G6U&lt;W^M:8)O&lt;(:D&lt;'&amp;T=Q!66EF3:82P&lt;WRF=CZM&gt;G.M98.T)'FO!"A!]!!#!!!!!1)!!!A!!!E!!!!1!!!!!!%!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_ShiftTerminalBounds.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_ShiftTerminalBounds.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$=!!!!#Q!,1!)!"'RF:H1!!!F!!A!$&gt;'^Q!!N!!A!&amp;=GFH;(1!$5!#!!:C&lt;X2U&lt;WU!!"R!5!!%!!!!!1!#!!--2X*P&gt;WYA1G^V&lt;G2T!!!A1%!!!@````]!"".5:8*N;7ZB&lt;#"#&lt;X6O:(-A&lt;X6U!"R!1!!"`````Q!%$V2F=GVJ&lt;G&amp;M)%*P&gt;7ZE=Q!&amp;!!-!!"2!1!!"`````Q!("UFO:'FD:8-!%U!$!!R4;'FG&gt;#""&lt;7^V&lt;H1!!#1!]!!%!!5!"A!)!!E#!!!A!!!.!1!##A!!!AA!!!!)!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
	</Item>
	<Item Name="CLSUIP_ShouldReplaceControl.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_ShouldReplaceControl.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!B$V.I&lt;X6M:#"S:8"M97.F0Q!%!!!!1E"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$&amp;:*5G6U&lt;W^M:8)A-A!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!=!$F2F=GVJ&lt;G&amp;M)%FO:'6Y!!!51(!!#!!!!$A!!!&gt;$&lt;WZ197ZF!%"!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!J736*F&gt;'^P&lt;'6S!!!]!0!!#!!$!!1!"1!'!!=!#!!*!!I#!!"A!!!."!!!#1!!!!!!!!#."Q!!#A!!!!A!!!!)!!!!EA!!!!!"!!M!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_StripExtension.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_StripExtension.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"3!!!!!Q!C1$$`````'(.U=GFO:S"X;82I&lt;X6U)'6Y&gt;'6O=WFP&lt;A!!%%!Q`````Q:T&gt;(*J&lt;G=!!"A!]!!#!!!!!1)!!!A!!!E!!!%+!!!!!!%!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
	</Item>
	<Item Name="CLSUIP_UnlockNewVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_UnlockNewVI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$^!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!#F:*5G6U&lt;W^M:8)!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!&amp;!!1!"!!'!Q!!?!!!$1A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!)!!!!!!%!"Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
	</Item>
	<Item Name="CLSUIP_UserScripting.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_UserScripting.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!Z736*F&gt;'^P&lt;'6S)'^V&gt;!!!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!"J!=!!)!!!!!A!!$&amp;:*)&amp;*F:GZV&lt;3"J&lt;A!!1%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!#F:*5G6U&lt;W^M:8)!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!#1!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_CalculateFutureParent.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_CalculateFutureParent.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!=!!)!!!!3A!!$5ZF&gt;V"B=G6O&gt;%FU:7U!2%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$F:*5G6U&lt;W^M:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%*!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!V736*F&gt;'^P&lt;'6S)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_Read Future Parent ProjectItem.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_Read Future Parent ProjectItem.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#:!=!!)!!!!3A!!'5:V&gt;(6S:3"198*F&lt;H1A5(*P;G6D&gt;%FU:7U!2%"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$F:*5G6U&lt;W^M:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%*!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!V736*F&gt;'^P&lt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_Write Future Parent ProjectItem.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_Write Future Parent ProjectItem.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!Z736*F&gt;'^P&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!G1(!!#!!!!%I!!"F'&gt;82V=G5A5'&amp;S:7ZU)&amp;"S&lt;WJF9X2*&gt;'6N!%*!=!!?!!!L&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)36EF3:82P&lt;WRF=CZM&gt;G.M98.T!!V736*F&gt;'^P&lt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="CLSUIP_CopyFrontPanelAndConPane.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_CopyFrontPanelAndConPane.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#A!!!!"1!%!!!!'%"Q!!A!!!!#!!!+4G6X)&amp;:*)'^V&gt;!!!&amp;%"Q!!A!!!!#!!!'4G6X)&amp;:*!!!51(!!#!!!!!)!!!&gt;0=GFH)&amp;:*!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!A!$!Q!!?!!!!!!!!!!!!!!!!!!!$1I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!!!%!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_CopyExecutionProperties.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_CopyExecutionProperties.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!=!!)!!!!!A!!#EZF&gt;S"733"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"Q!!A!!!!#!!!*4WRE)&amp;:*)'FO!":!=!!)!!!!!A!!#5ZF&gt;S"733"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="CLSUIP_IsOldVIOwnedByInterface.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/VIRetooler/CLSUIP_IsOldVIOwnedByInterface.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#U!!!!"!!%!!!!%E!B$'FT)'FO&gt;'6S:G&amp;D:1!!1E"Q!"Y!!#M7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B*736*F&gt;'^P&lt;'6S,GRW9WRB=X-!$6:*5G6U&lt;W^M:8)A;7Y!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!)$!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!!!!1!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</LVClass>
